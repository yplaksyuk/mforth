# Name: Makefile
# Author: Yuri Plaksyuk <yuri.plaksyuk@gmail.com>
# License: See LICENSE file

PART       = m8
DEVICE     = atmega8
CLOCK      = 8000000
PROGRAMMER = -c usbasp -P usb -B 50
FUSES      = -U lfuse:w:0xE4:m -U hfuse:w:0xD9:m
OBJECTS    = mforth.o

# Tune the lines below only if you know what you are doing:
# Fuse calc: http://www.engbedded.com/fusecalc/
AVRDUDE = avrdude $(PROGRAMMER) -p $(PART)
COMPILE = avr-gcc -Wall -Os -DF_CPU=$(CLOCK) -mmcu=$(DEVICE)
OBJCOPY = avr-objcopy
OBJDUMP = avr-objdump
OBJSIZE = avr-size

# symbolic targets:
all: mforth.hex

.S.o:
	$(COMPILE) -x assembler-with-cpp -c $< -o $@
# "-x assembler-with-cpp" should not be necessary since this is the default
# file type for the .S (with capital S) extension. However, upper case
# characters are not always preserved on Windows. To ensure WinAVR
# compatibility define the file type manually.

.c.s:
	$(COMPILE) -S $< -o $@

flash:	all
	$(AVRDUDE) -U flash:w:mforth.hex:i

fuse:
	$(AVRDUDE) $(FUSES)

check:
	$(AVRDUDE)

# Xcode uses the Makefile targets "", "clean" and "install"
install: flash fuse

clean:
	rm -f mforth.hex mforth.elf $(OBJECTS)

# file targets:
mforth.elf: $(OBJECTS)
	avr-ld -o mforth.elf $(OBJECTS)
	$(OBJSIZE) mforth.elf

mforth.hex: mforth.elf
	rm -f mforth.hex
	avr-objcopy -j .text -O ihex mforth.elf mforth.hex
	avr-objdump -j .text -s mforth.elf > mforth.bin

#	avr-size --format=avr --mcu=$(DEVICE) main.elf
# If you have an EEPROM section, you must also create a hex file for the
# EEPROM and add it to the "flash" target.

# Targets for code debugging and analysis:
disasm:	mforth.elf
	$(OBJDUMP) -d mforth.elf
